import sys
from os import listdir
from os.path import isfile, join, split
import requests
import logging


logger = logging.getLogger()
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logger.addHandler(console)
logger.setLevel(logging.INFO)


url = "http://127.0.0.1:5000"
endpoint = "images"
full_url = join(url, endpoint)


def upload_image(path, connect_timeout=5, read_timeout=5):
    """Считаем, что сервер отвечает json'ом с полями success, reason (в случае неудачной загрузки),
    key (ключ, по которому потом можно будет достать эту картинку с сервера в случае успеха).
    """
    _, filename = split(path)

    # Считаем, что сервер возвращает уникальный ключ, по которому потом можно достать картинку get-запросом, например.
    key = None
    try:
        f_in = open(path, 'rb')
        image = f_in.read()
    except OSError:
        logger.exception('Cannot open file ' + path)
        return key
    else:
        f_in.close()

    try:
        files = {'image': (filename, image)}
        response = requests.post(full_url, files=files, timeout=(connect_timeout, read_timeout))
        response_json = response.json()
        if not response_json["success"]:
            logger.error("Could not upload image {0}. Reason: " + response_json["reason"])
        else:
            key = response_json['key']
            logger.info("Status code: " + str(response.status_code) + " Successfully uploaded image " + filename)
    except KeyError:
        logger.exception("Key not found in response json.")
    except ValueError:
        logger.exception("Response body is not a valid json.")
    except requests.exceptions.RequestException:
        logger.exception("Requests exception.")

    return key


def upload_all(path_to_folder, connect_timeout=5, read_timeout=5, retry=3):
    only_files = [join(path_to_folder, f) for f in listdir(path_to_folder) if isfile(join(path_to_folder, f))]

    successful_keys = {}
    fails = set()
    for f_path in only_files:
        key = None
        for i in range(retry):
            key = upload_image(f_path, connect_timeout, read_timeout)
            if key is not None:
                break

        if key is None:
            logger.error("Could not upload {0} in {1} attempts".format(f_path, retry))
            fails.add(f_path)
        else:
            successful_keys[f_path] = key
    if fails:
        logger.error("Some files were not uploaded.")

    return successful_keys, fails


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise ValueError("You must specify folder path.")

    folder_path = sys.argv[1]
    upload_all(folder_path)
