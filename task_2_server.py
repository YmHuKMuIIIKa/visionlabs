import os
from flask import Flask, request, jsonify
from random import randint

UPLOAD_FOLDER = 'uploads'
os.makedirs(UPLOAD_FOLDER, exist_ok=True)

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/images', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files["image"]
        name = file.filename
        key = 3
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], name))

    #  Рандом здесь имитирует создание настоящего уникального ключа для загруженной картинки, по которому
    #  мы бы могли ее получить позднее.
    return jsonify({"success": True, "key": randint(0, 100000000)}), 200


if __name__ == '__main__':
    app.run(debug=True)
