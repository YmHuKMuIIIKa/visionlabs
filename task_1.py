import json

FLOAT_PATTERN = "{0:.{1}f}"
JSON_SIMPLE_TYPES = [bool, int, type(None), str]


def is_equal_floats(first, second, float_precision=5):
    """Крайне медленная строковая реализация сравнения float'ов.
    float_precision задает число значащих знаков после десятичной точки.
    """
    # увеличиваем точность на 1, чтобы избежать изменения последнего знака из-за округления.
    first_str = FLOAT_PATTERN.format(first, float_precision + 1)[:-1]
    second_str = FLOAT_PATTERN.format(second, float_precision + 1)[:-1]

    # Корректно обрабатываются случаи типа first == second == nan
    # (в отличие от стандарта, считает это сравнение истинным).
    # Также корректно сравнивает +-Infinity.
    #
    # Кстати. замечу, что если мы сравниваем два dict'а на равенство через ==, то они тоже трактуют NaN'ы
    # как одинаковые значения.
    return first_str == second_str


def is_equal_jsons(first, second, float_precision=5):
    """Сравнивает два объекта, полученных с помощью функции json.loads или json.load.

    Из условия задачи было не совсем понятно, как сравнивать флоаты. Либо использовать пять старших цифр,
    либо же имелась в виду точность 5 знаков после точки. Если я реализовал не тот вариант, то это всегда
    можно оперативно исправить."""
    first_type, second_type = type(first), type(second)
    if first_type != second_type:
        return False

    if first_type in JSON_SIMPLE_TYPES:
        return first == second

    if first_type == float:
        return is_equal_floats(first, second, float_precision)

    if first_type == list:
        if len(first) != len(second):
            return False
        return all(is_equal_jsons(first[i], second[i], float_precision) for i in range(len(first)))

    if first_type == dict:
        first_keys = list(sorted(first))
        second_keys = list(sorted(second))
        if first_keys != second_keys:
            return False
        return all(is_equal_jsons(first[key], second[key], float_precision) for key in first_keys)

    return True
